package com.example.young.beacon;

/**
 * Created by Young on 2017-07-08.
 */
//그것 밖에 못했냐고 하셨다.
public class ListViewItem {
    private int no ;
    private String text ;

    public void setNo(int no) {
        this.no = no ;
    }
    public void setText(String text) {
        this.text = text ;
    }

    public int getNo() {
        return no ;
    }
    public String getText() {
        return text ;
    }

}
