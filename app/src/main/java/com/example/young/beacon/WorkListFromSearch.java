package com.example.young.beacon;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class WorkListFromSearch extends Activity {
    private ArrayAdapter majorAdapter;
    private Spinner majorSpinner;
    private Work_List_Dialog mWorkListDialog;
    private String workName;
    private String major;
    private String userID;
    private ListView workListView;
    private WorkListAdapter workListAdapter;
    private List<WorkList> workList;


    ArrayList<ListViewAdapter> itemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_list_from_search);

        //작품 리스트
        workListView = (ListView)findViewById(R.id.workListView);
        workList = new ArrayList<WorkList>();
        TextView textView = (TextView)findViewById(R.id.workName) ;
        workListAdapter = new WorkListAdapter(getApplicationContext(),workList);
        workListView.setAdapter(workListAdapter);
        Intent intent = getIntent();
        major = intent.getExtras().getString("major");
        userID = intent.getExtras().getString("userID");
        new WorkListFromSearch.BackgroundTask().execute();


        workListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                workName = workList.get(i).getWorkName();
                Intent intent = new Intent(WorkListFromSearch.this, WorkInfo.class);
                intent.putExtra("Name",workName);
                intent.putExtra("userID",userID);
                startActivity(intent);
            }
        });
    }



    class BackgroundTask extends AsyncTask<Void, Void, String>
    {


        String target;
        @Override
        protected void onPreExecute(){
            try {

                target = "http://192.168.200.174/WorkListForAppraisal.php?major="+ URLEncoder.encode(major,"UTF-8");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                URL url = new URL(target);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp;
                StringBuilder stringBuilder = new StringBuilder();
                while ((temp = bufferedReader.readLine()) != null)
                {
                    stringBuilder.append(temp +"\n");
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        public void onProgressUpdate(Void... values){
            super.onProgressUpdate();
        }
        @Override
        public void onPostExecute(String result){
            try{
                workList.clear();
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                int count = 0;

                String workName;
                String average;
                String major;

                    while (count < jsonArray.length()){

                        JSONObject object = jsonArray.getJSONObject(count);
                        workName = object.getString("workName");
                        average = object.getString("average");
                        major = object.getString("major");
                        WorkList workNameList = new WorkList( workName, average, major);
                        workList.add(workNameList);
                        count++;

                    if (count ==0){
                        AlertDialog dialog;
                        AlertDialog.Builder builder = new AlertDialog.Builder(WorkListFromSearch.this);
                        dialog = builder.setMessage("조회된 댓글 없음")
                                .setPositiveButton("확인",null)
                                .create();
                        dialog.show();
                    }
                    workListAdapter.notifyDataSetChanged();
                }
           /*     AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(CommentList.this);
                dialog = builder.setMessage(result)
                        .setPositiveButton("확인",null)
                        .create();
                dialog.show();*/
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}