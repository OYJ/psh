package com.example.young.beacon;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by young on 2017-09-21.
 */
// 1학기 캡스톤 디자인 성적은
public class AppraisalRequest extends StringRequest {

    final static private String URL = "http://192.168.200.174/Appraisal.php";
    private Map<String, String> parameters;

    public AppraisalRequest(String num, String userID,String major, String workName, String average, String blank , Response.Listener<String> listener) {
        super(Method.POST, URL, listener, null);
        parameters = new HashMap<>();
        parameters.put("num", num);
        parameters.put("userID", userID);
        parameters.put("major", major);
        parameters.put("workName", workName);
        parameters.put("average", average);
        parameters.put("blank", blank);
    }

    @Override
    public Map<String, String> getParams() {
        return parameters;
    }
}
