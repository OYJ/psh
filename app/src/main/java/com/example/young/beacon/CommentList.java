package com.example.young.beacon;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class CommentList extends AppCompatActivity {
    private String workComment;
//계획서를 보고 평가하는 수업이었으면

    private ListView commentListView;
    private  CommentListAdapter adapter;
    private List<Comment> commentList;
   //인텐트 값 받아오기(여기서는 workName 받아옴)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_list);

        commentListView = (ListView)findViewById(R.id.commentListView);
        commentList = new ArrayList<Comment>();
        adapter = new CommentListAdapter(getApplicationContext(),commentList);
        commentListView.setAdapter(adapter);
        new BackgroundTask().execute();

}

    class BackgroundTask extends AsyncTask<Void, Void, String>
    {
        //작품명 받아옴 변수 workName
        Intent intent = getIntent();
        String workName = intent.getExtras().getString("workName");
        String target;

        @Override
        protected void onPreExecute(){
            try {
                target = "http://192.168.200.174/CommentList.php?workName="+URLEncoder.encode(workName,"UTF-8");
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                URL url = new URL(target);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp;
                StringBuilder stringBuilder = new StringBuilder();
                while ((temp = bufferedReader.readLine()) != null)
                {
                    stringBuilder.append(temp +"\n");
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        public void onProgressUpdate(Void... values){
            super.onProgressUpdate();
        }
        @Override
        public void onPostExecute(String result){
            try{
                commentList.clear();
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                int count = 0;

                String num;
                String workComment;
                String workName;
                String userID;
                String star;
                while (count < jsonArray.length()){

                 JSONObject object = jsonArray.getJSONObject(count);
                    num = object.getString("num");
                    workComment = object.getString("workComment");
                    workName = object.getString("workName");
                    star = object.getString("star");
                    userID = object.getString("userID");
                    Comment comment = new Comment(num, workComment,  workName,  userID,  star);
                    commentList.add(comment);
                    count++;

                    if (count ==0){
                        AlertDialog dialog;
                        AlertDialog.Builder builder = new AlertDialog.Builder(CommentList.this);
                        dialog = builder.setMessage("조회된 댓글 없음")
                                .setPositiveButton("확인",null)
                                .create();
                        dialog.show();
                    }
                adapter.notifyDataSetChanged();
                }
           /*     AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(CommentList.this);
                dialog = builder.setMessage(result)
                        .setPositiveButton("확인",null)
                        .create();
                dialog.show();*/
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
