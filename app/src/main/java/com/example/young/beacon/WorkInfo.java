package com.example.young.beacon;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class WorkInfo extends AppCompatActivity {

     ImageView workInfo;
     String workName;
    private Button commentList;
    private Button comment;
    String Url;
    Bitmap bmImg;
    back task;
    private String UI;
private String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_info);


       Button commentList = (Button)findViewById(R.id.commentList);
        Button  comment = (Button)findViewById(R.id.comment);
        //Url= "http://192.168.200.181/hknus.jpg";

        GlobalString gUserID = (GlobalString)getApplication();
        UI = gUserID.getGuserID();

        Intent intent = getIntent();
        workName = intent.getExtras().getString("Name");
        task = new back();
        workInfo = (ImageView) findViewById(R.id.workInfo);

        comment.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WorkInfo.this, CommentActivity.class);
                intent.putExtra("workName",workName);
                startActivity(intent);
            }
        });
        commentList.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WorkInfo.this, CommentList.class);
                intent.putExtra("workName",workName);
                startActivity(intent);
            }
        });

    if (workName.equals("바이오 칼라")){
        name ="bc";
        Url = "http://192.168.200.174/bc.jpg";
    }
     if (workName.equals("Mini Baja Off-road Vehicle")){
        name="mborv";
        Url = "http://192.168.200.174/mborv.jpg";
    }
    if (workName.equals("스마트 지팡이") ){
        name="ss";
        Url = "http://192.168.200.174/ss.jpg";
    }
    if (workName.equals("스마트폰 찾기 어플리케이션") ){
        name="ssa";
        Url = "http://192.168.200.174/ssa.jpg";
    }
   if (workName.equals("스트리밍 자동 급식기")){
        name="saf";
        Url = "http://192.168.200.174/saf.jpg";
    }
     if (workName.equals("3D game with Myo") ){
        name="gwm";
        Url = "http://192.168.200.174/gwm.jpg";
    }
   if (workName.equals("차선 인식")){
        name="la";
        Url = "http://192.168.200.174/la.jpg";
    }
     if (workName.equals("Smart Home")){
        name="sh";
        Url = "http://192.168.200.174/sh.jpg";
    }
     if (workName.equals("솔레노이드 벨브를 이용한 자동배수 시스템")){
        name="sbuas";
        Url = "http://192.168.200.174/sbuas.jpg";
    }
     if (workName.equals( "IoT 기반 주차 서비스")){
        name="iot";
        Url = "http://192.168.200.174/iot.jpg";
    }
     if (workName.equals("스마트 줄서기")){
        name="sl";
        Url = "http://192.168.200.174/sl.jpg";
    }
     if (workName.equals("블루투스 통신을 이용한 환자 관리 시스템")){
        name="bms";
        Url = "http://192.168.200.174/bms.jpg";
    }
     if (workName.equals("한경대학교 시설예약 시스템")){
        name="hknus";
        Url = "http://192.168.200.174/hknus.jpg";
    }
     if (workName.equals("VIDEO")){
        name="video";
           Url = "http://192.168.200.174/video.jpg";
    }
    if (workName.equals("UB-EYE")){
        name="ue";
        Url = "http://192.168.200.174/ue.jpg";
    }
     if (workName.equals("Score LOOK")){
        name="scorelook";
        Url = "http://192.168.200.174/scorelook.jpg";
    }
     if (workName.equals("길이조절 안전발판")){
        name="lcsf";
        Url = "http://192.168.200.174/lcsf.jpg";
    }
     if (workName.equals("경보 안전모")){
        name="ssh";
        Url = "http://192.168.200.174/ssh.jpg";
    }
     if (workName.equals("HIT ROOL")){
        name="hitrool";
        Url = "http://192.168.200.174/hitrool.jpg";
    }
     if (workName.equals("남은 향에 취하다") ){
        name="ab";
        Url = "http://192.168.200.174/ab.jpg";
    }


        task.execute(Url);
}

    private class back extends AsyncTask<String, Integer,Bitmap> {



        @Override
        protected Bitmap doInBackground(String... urls) {
            // TODO Auto-generated method stub
            try{
                URL myFileUrl = new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection)myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();

                InputStream is = conn.getInputStream();

                bmImg = BitmapFactory.decodeStream(is);


            }catch(IOException e){
                e.printStackTrace();
            }
            return bmImg;
        }

        protected void onPostExecute(Bitmap img){
            workInfo.setImageBitmap(bmImg);
        }

    }
}



