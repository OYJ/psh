package com.example.young.beacon;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by young on 2017-08-24.
 */

public class WorkListAdapter extends BaseAdapter {

    private Context context;
    private List<WorkList>  workList;

    public WorkListAdapter(Context context, List<WorkList> workList) {
        this.context = context;
        this.workList = workList;
    }

    @Override
    public int getCount() {
        return workList.size();
    }

    @Override
    public Object getItem(int i) {
        return workList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View v = View.inflate(context, R.layout.work, null);
        TextView workText = (TextView)v.findViewById(R.id.workName);
        TextView averageText = (TextView)v.findViewById(R.id.average);
        TextView majorText = (TextView)v.findViewById(R.id.major8);


        workText.setText(workList.get(i). getWorkName());
        averageText.setText(workList.get(i). getAverage());
        majorText.setText(workList.get(i). getMajor());

        v.setTag(workList.get(i).getWorkName());
        return v;
    }
}
