package com.example.young.beacon;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.perples.recosdk.RECOBeacon;
import com.perples.recosdk.RECOBeaconRegion;
import com.perples.recosdk.RECOErrorCode;
import com.perples.recosdk.RECORangingListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;


public class WorkSearch extends RecoActivity implements RECORangingListener {

    private ArrayList<RECOBeaconRegion> rangingRegions;
    private RecoRangingListAdapter mRangingListAdapter;
    private ListView mRegionListView;
    private TextView[] major;
    public String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
       /* Intent intent = getIntent();
        userID = intent.getExtras().getString("userID");*/

        major = new TextView[21];
        major[1] = (TextView) findViewById(R.id.major1);
        major[2] = (TextView) findViewById(R.id.major2);
        major[3] = (TextView) findViewById(R.id.major3);
        major[4] = (TextView) findViewById(R.id.major4);
        major[5] = (TextView) findViewById(R.id.major5);
        major[6] = (TextView) findViewById(R.id.major6);
        major[7] = (TextView) findViewById(R.id.major7);
        major[8] = (TextView) findViewById(R.id.major8);
        major[9] = (TextView) findViewById(R.id.major9);
        major[10] = (TextView) findViewById(R.id.major10);
        major[11] = (TextView) findViewById(R.id.major11);
        major[12] = (TextView) findViewById(R.id.major12);
        major[13] = (TextView) findViewById(R.id.major13);
        major[14] = (TextView) findViewById(R.id.major14);
        major[15] = (TextView) findViewById(R.id.major15);
        major[16] = (TextView) findViewById(R.id.major16);
        major[17] = (TextView) findViewById(R.id.major17);
        major[18] = (TextView) findViewById(R.id.major18);
        major[19] = (TextView) findViewById(R.id.major19);
        major[20] = (TextView) findViewById(R.id.major20);
//---------------------------지도 버튼 이벤트 START ----------------------------
          major[1].setOnClickListener(new View.OnClickListener() {
               @Override
             public void onClick(View v) {
                 majorClick(major[1].getText().toString());
             }
           });
        major[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[2].getText().toString());
            }
        });

        major[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[3].getText().toString());
            }
        });

        major[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[4].getText().toString());
            }
        });

        major[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[5].getText().toString());
            }
        });

        major[6].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[6].getText().toString());
            }
        });

        major[7].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[7].getText().toString());
            }
        });

        major[8].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[8].getText().toString());
            }
        });

        major[9].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[9].getText().toString());
            }
        });

        major[10].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[10].getText().toString());
            }
        });

        major[11].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[11].getText().toString());
            }
        });

        major[12].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[12].getText().toString());
            }
        });

        major[13].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[13].getText().toString());
            }
        });

        major[14].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[14].getText().toString());
            }
        });

        major[15].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[15].getText().toString());
            }
        });

        major[16].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[16].getText().toString());
            }
        });
        major[17].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[17].getText().toString());
            }
        });

        major[18].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[18].getText().toString());
            }
        });
        major[19].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[19].getText().toString());
            }
        });
        major[20].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majorClick(major[20].getText().toString());
            }
        });



//---------------------------지도 버튼 이벤트 END ----------------------------

        //mRecoManager will be created here. (Refer to the RECOActivity.onCreate())
        //mRecoManager 인스턴스는 여기서 생성됩니다. RECOActivity.onCreate() 메소들르 참고하세요.

        //Set RECORangingListener (Required)
        //RECORangingListener 를 설정합니다. (필수)
        mRecoManager.setRangingListener(this);

        /**
         * Bind RECOBeaconManager with RECOServiceConnectListener, which is implemented in RECOActivity
         * You SHOULD call this method to use monitoring/ranging methods successfully.
         * After binding, onServiceConenct() callback method is called.
         * So, please start monitoring/ranging AFTER the CALLBACK is called.
         *
         * RECOServiceConnectListener와 함께 RECOBeaconManager를 bind 합니다. RECOServiceConnectListener는 RECOActivity에 구현되어 있습니다.
         * monitoring 및 ranging 기능을 사용하기 위해서는, 이 메소드가 "반드시" 호출되어야 합니다.
         * bind후에, onServiceConnect() 콜백 메소드가 호출됩니다. 콜백 메소드 호출 이후 monitoring / ranging 작업을 수행하시기 바랍니다.
         */
        mRecoManager.bind(this);

    }





    @Override
    protected void onResume() {
        super.onResume();

        mRangingListAdapter = new RecoRangingListAdapter(this);
       /* mRegionListView = (ListView)findViewById(R.id.list_ranging);
        mRegionListView.setAdapter(mRangingListAdapter);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.stop(mRegions);
        this.unbind();
    }

    private void unbind() {
        try {
            mRecoManager.unbind();
        } catch (RemoteException e) {
            Log.i("RECORangingActivity", "Remote Exception");
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceConnect() {
/*        //리스트 생성해서 비콘 값 저장
        rangingRegions = new ArrayList<RECOBeaconRegion>();
        //비콘의UUID, Major, 이름
        Log.i("RECORangingActivity", "onServiceConnect()");
        mRecoManager.setDiscontinuousScan(MainActivity_stu.DISCONTINUOUS_SCAN);
        this.start(mRegions);
        //Write the code when RECOBeaconManager is bound to RECOBeaconService*/
        //리스트 생성해서 비콘 값 저장
        rangingRegions = new ArrayList<RECOBeaconRegion>();
        //비콘의UUID, Major, 이름 비콘등록
        rangingRegions.add(new RECOBeaconRegion("24DDF411-8CF1-440C-87CD-E368DAF9C91E",501, "1번"));
        rangingRegions.add(new RECOBeaconRegion("24DDF411-8CF1-440C-87CD-E368DAF9C92E",502, "2번"));




        for (RECOBeaconRegion region : rangingRegions) {
            try {
                mRecoManager.startRangingBeaconsInRegion(region);
                mRecoManager.requestStateForRegion(region);

            } catch (RemoteException e) {
                // RemoteException 발생 시 작성 코드
            } catch (NullPointerException e) {
                // NullPointerException 발생 시 작성 코드

            }
        }
    }

    /*   @Override
       public void didRangeBeaconsInRegion(Collection<RECOBeacon> recoBeacons, RECOBeaconRegion recoRegion) {
     *//*      Log.i("RECORangingActivity", "didRangeBeaconsInRegion() region: " + recoRegion.getUniqueIdentifier() + ", number of beacons ranged: " + recoBeacons.size());
        mRangingListAdapter.updateAllBeacons(recoBeacons);
        mRangingListAdapter.notifyDataSetChanged();

        TextView textView = (TextView)findViewById(R.id.text5);
        textView.setText(recoRegion.getMinor()+"");
        //Write the code when the beacons in the region is received*//*

    }*/

    @Override
    public void didRangeBeaconsInRegion(Collection<RECOBeacon> collection, RECOBeaconRegion recoBeaconRegion) {
        // ranging중인 region에서, 1초 간격으로 변경사항을 받아 이 callback을 부름
        //이곳은 여름방학에 못했.. 아니 교수님이 말한 그것밖에 못했서 못했던 부분 ㅇㅇ


        if(collection.size() < 1){
          for (int i = 1 ; i  < 21 ; i++ ){
                major[i].setClickable(false);
               // major[i].setBackgroundColor(Color.WHITE);
                  major[i].setBackgroundResource(R.drawable.border);
             }
        } else
        {
                ArrayList<RECOBeacon> mRangedBeacons = new ArrayList<RECOBeacon>(collection);

            for (RECOBeacon beacon : mRangedBeacons) {
                if (beacon.getMajor()==502){
                    Log.i("RECORangingActivity", "비콘메이저 " + beacon.getMajor());
                    major[2].setBackgroundColor(Color.RED);
                    major[2].setClickable(true);
                }
                if (beacon.getMajor()==501){
                    Log.i("RECORangingActivity", "비콘메이저 " + beacon.getMajor());
                    major[1].setBackgroundColor(Color.RED);
                    major[1].setClickable(true);
                }

            }



        }

    }
    @Override
    protected void start(ArrayList<RECOBeaconRegion> regions) {

        /**
         * There is a known android bug that some android devices scan BLE devices only once. (link: http://code.google.com/p/android/issues/detail?id=65863)
         * To resolve the bug in our SDK, you can use setDiscontinuousScan() method of the RECOBeaconManager.
         * This method is to set whether the device scans BLE devices continuously or discontinuously.
         * The default is set as FALSE. Please set TRUE only for specific devices.
         *
         * mRecoManager.setDiscontinuousScan(true);
         */

        for(RECOBeaconRegion region : regions) {
            try {
                mRecoManager.startRangingBeaconsInRegion(region);
            } catch (RemoteException e) {
                Log.i("RECORangingActivity", "Remote Exception");
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.i("RECORangingActivity", "Null Pointer Exception");
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void stop(ArrayList<RECOBeaconRegion> regions) {
        for(RECOBeaconRegion region : regions) {
            try {
                mRecoManager.stopRangingBeaconsInRegion(region);
            } catch (RemoteException e) {
                Log.i("RECORangingActivity", "Remote Exception");
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.i("RECORangingActivity", "Null Pointer Exception");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onServiceFail(RECOErrorCode errorCode) {
        //Write the code when the RECOBeaconService is failed.
        //See the RECOErrorCode in the documents.
        return;
    }

    @Override
    public void rangingBeaconsDidFailForRegion(RECOBeaconRegion region, RECOErrorCode errorCode) {
        Log.i("RECORangingActivity", "error code = " + errorCode);
        //Write the code when the RECOBeaconService is failed to range beacons in the region.
        //See the RECOErrorCode in the documents.
        return;
    }

public void majorClick(String ma){
    Intent intent = new Intent(WorkSearch.this, WorkListFromSearch.class);
    intent.putExtra("major", ma);
    intent.putExtra("userID",userID);
    startActivity(intent);
}

  /*  private TextView tv;
    private ArrayList<RECOBeaconRegion> rangingRegions;
    boolean mScanRecoOnly = true;
    boolean mEnableBackgroundTimeout = true;
    RECOBeaconManager recoManager = RECOBeaconManager.getInstance(this, mScanRecoOnly, mEnableBackgroundTimeout);

    public WorkSearch() {
    }

    *//*    recoManager.bind(this);
        recoManager.unbind();*//*
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.work_search);
            tv = (TextView)findViewById(R.id.textView);
            tv.setText("0");
            recoManager = RECOBeaconManager.getInstance(this, true, true);
            recoManager.bind(this);
            recoManager.setRangingListener(this);

    }

    public void onServiceConnect() {
        //RECOBeaconService와 연결 시 코드 작성

        tv.setText("1");

        //리스트 생성해서 비콘 값 저장
        rangingRegions = new ArrayList<RECOBeaconRegion>();
        //비콘의UUID, Major, 이름
        rangingRegions.add(new RECOBeaconRegion("24DDF411-8CF1-440C-87CD-E368DAF9C91E", 501, "1번"));
        rangingRegions.add(new RECOBeaconRegion("24DDF411-8CF1-440C-87CD-E368DAF9C92E", 502, "2번"));


        for (RECOBeaconRegion region : rangingRegions) {
            try {
                recoManager.startRangingBeaconsInRegion(region);
                recoManager.requestStateForRegion(region);

            } catch (RemoteException e) {
                // RemoteException 발생 시 작성 코드
            } catch (NullPointerException e) {
                // NullPointerException 발생 시 작성 코드

            }
        }

    }


    @Override
    public void onServiceFail(RECOErrorCode recoErrorCode) {

    }

    *//**
     * ------------------------- 2. Ranging callbacks -------------------------
     *//*

    @Override
    public void didRangeBeaconsInRegion(Collection<RECOBeacon> collection, RECOBeaconRegion recoBeaconRegion) {
        // ranging중인 region에서, 1초 간격으로 변경사항을 받아 이 callback을 부름

        *//*tv.append("ranging..! - ");*//*

        if(collection.size() == 0){
            tv.append(recoBeaconRegion.getUniqueIdentifier() + " 안잡힘");
        } else
        {
            if (recoBeaconRegion.getUniqueIdentifier().equals("1번") || recoBeaconRegion.getUniqueIdentifier().equals("2번")
                    ||recoBeaconRegion.getUniqueIdentifier().equals("3번")) {
                TextView proxText = (TextView) findViewById(R.id.textView);


                ArrayList<RECOBeacon> mRangedBeacons = new ArrayList<RECOBeacon>(collection);

                for (RECOBeacon beacon : mRangedBeacons) {

                    proxText.append(""+beacon.getMajor());

                }
            }

        }

    }

    @Override
    public void rangingBeaconsDidFailForRegion(RECOBeaconRegion recoBeaconRegion, RECOErrorCode recoErrorCode) {
        // ranging 실패시
        tv.append("ranging 실패... ㅠ.ㅠ \n");
    }
*/
}


