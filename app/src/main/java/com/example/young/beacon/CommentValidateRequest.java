package com.example.young.beacon;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by young on 2017-08-30.
 */

public class CommentValidateRequest extends StringRequest {
//잘 모르지만 꾸역꾸역 했지만
    final static private String URL = "http://192.168.200.174/CommentValidate.php";
    private Map<String, String> parameters;

    public CommentValidateRequest(String userID, String workName, Response.Listener<String> listener){
        super(Method.POST, URL, listener, null);
        parameters= new HashMap<>();
        parameters.put("userID", userID);
        parameters.put("workName", workName);
    }

    @Override
    public Map<String, String> getParams(){
        return parameters;
    }

}
