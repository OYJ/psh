package com.example.young.beacon;

import android.app.Application;

/**
 * Created by young on 2017-09-18.
 */

public class GlobalString extends Application {
// 1학기 성적은 B+이었다.
    private String  GuserID;
    private String Gposition;
    private String GworkName;
    private String Gmajor;
     private String A1;
    private String A2;
    private String A3;
    private String A4;
    private String A5;
    private String A6;

    public String getGposition() {
        return Gposition;
    }

    public void setGposition(String gposition) {
        Gposition = gposition;
    }

    public String getGuserID() {
        return GuserID;
    }

    public void setGuserID(String guserID) {
        GuserID = guserID;
    }

    public String getGmajor() {
        return Gmajor;
    }
    public void setGmajor(String gmajor) {
        Gmajor = gmajor;
    }
    public String getGworkName() {
        return GworkName;
    }
    public void setGworkName(String gworkName) {
        GworkName = gworkName;
    }

    public String getA1() {
        return A1;
    }

    public void setA1(String a1) {
        A1 = a1;
    }

    public String getA2() {
        return A2;
    }

    public void setA2(String a2) {
        A2 = a2;
    }

    public String getA3() {
        return A3;
    }

    public void setA3(String a3) {
        A3 = a3;
    }

    public String getA4() {
        return A4;
    }

    public void setA4(String a4) {
        A4 = a4;
    }

    public String getA5() {
        return A5;
    }

    public void setA5(String a5) {
        A5 = a5;
    }

    public String getA6() {
        return A6;
    }

    public void setA6(String a6) {
        A6 = a6;
    }



}
