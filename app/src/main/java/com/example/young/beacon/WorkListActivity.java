package com.example.young.beacon;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WorkListActivity extends Activity {
    private ArrayAdapter majorAdapter;
    private Spinner majorSpinner;
    private Work_List_Dialog mWorkListDialog;
    ListViewAdapter adapter;

    private String workName;
    private String major;
    private ListView workListView;
    private WorkListAdapter workListAdapter;
    private List<WorkList> workList;

    ArrayList<ListViewAdapter> itemList = new ArrayList<>();

                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.activity_work_list);

                    //작품 리스트

                    workListView = (ListView)findViewById(R.id.workListView);
                    workList = new ArrayList<WorkList>();
                    //workList.add(new WorkList("작품명","평점","전공"));
                    TextView textView = (TextView)findViewById(R.id.workName) ;
                    workListAdapter = new WorkListAdapter(getApplicationContext(),workList);
                    workListView.setAdapter(workListAdapter);
                    //검색
                    Button listSearch = (Button)findViewById(R.id.listSearchButton) ;
                    majorSpinner = (Spinner)findViewById(R.id.majorSpinner);
                    majorAdapter = ArrayAdapter.createFromResource(this, R.array.major, android.R.layout.simple_spinner_dropdown_item);
                    majorSpinner.setAdapter(majorAdapter);

                    Intent intent = getIntent();
                    major = "컴공과";

                    listSearch.setOnClickListener(new Button.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (majorSpinner.getSelectedItem().equals("[전공 선택]")){
                                Toast toast = Toast.makeText(getApplicationContext(), "전공을 선택해 주세요.", Toast.LENGTH_LONG);
                                toast.show();
                            }
                            else { //전공 추가하는 곳
                                if (majorSpinner.getSelectedItem().equals("전체")){
                                    major = "*";
                                    new BackgroundTask().execute();
                                }
                                else if (majorSpinner.getSelectedItem().equals("건축과")){
                                        major = majorSpinner.getSelectedItem().toString();
                                    new BackgroundTask().execute();
                                }
                                else if (majorSpinner.getSelectedItem().equals("컴공과")){
                                    major = majorSpinner.getSelectedItem().toString();
                                    new BackgroundTask().execute();
                                }
                                else if (majorSpinner.getSelectedItem().equals("기계과")){
                                    major = majorSpinner.getSelectedItem().toString();
                                    new BackgroundTask().execute();
                                }
                                else if (majorSpinner.getSelectedItem().equals("식공과")){
                                    major = majorSpinner.getSelectedItem().toString();
                                    new BackgroundTask().execute();
                                }
                                else if (majorSpinner.getSelectedItem().equals("전전제")){
                                    major = majorSpinner.getSelectedItem().toString();
                                    new BackgroundTask().execute();
                                }
                                else if (majorSpinner.getSelectedItem().equals("토안환")){
                                    major = majorSpinner.getSelectedItem().toString();
                                    new BackgroundTask().execute();
                                }
                            }
                        }
                    });



workListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
        workName = workList.get(i).getWorkName();
        dialog(workName);
           }
    });
   }
   //onCreate 끝


// 작품 눌렀을때 다이얼로그 작동
   public void dialog(String string)
   {
       mWorkListDialog = new Work_List_Dialog(this,
               "[다이얼로그 제목]", // 제목
               workName, // 내용
               leftListener, // 왼쪽 버튼 이벤트
               rightListener); // 오른쪽 버튼 이벤트
       mWorkListDialog.show();
   }
    private View.OnClickListener leftListener = new View.OnClickListener() {
        public void onClick(View v) { //왼쪽 버튼은 Comment 보기
            Intent intent = new Intent(WorkListActivity.this, CommentList.class);
            intent.putExtra("workName",workName);
            startActivity(intent);
            mWorkListDialog.dismiss();
        }
    };
    private View.OnClickListener rightListener = new View.OnClickListener() {
        public void onClick(View v) { //오른쪾 버튼은 Work 보기
            Intent intent = new Intent(WorkListActivity.this, WorkInfo.class);
            intent.putExtra("Name",workName);
            startActivity(intent);
            mWorkListDialog.dismiss();
         /*   Intent intent = new Intent(WorkListActivity.this, CommentList.class);
            intent.putExtra("workName",workName);
            startActivity(intent);
            mWorkListDialog.dismiss();*/
        }
    };

    class BackgroundTask extends AsyncTask<Void, Void, String>
    {
        String target;

        @Override
        protected void onPreExecute(){
            try {


        target = "http://192.168.200.174/WorkList.php?major="+ URLEncoder.encode(major,"UTF-8");
    }catch (Exception e){
        e.printStackTrace();
    }
}

        @Override
        protected String doInBackground(Void... params) {

            try {
                URL url = new URL(target);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp;
                StringBuilder stringBuilder = new StringBuilder();
                while ((temp = bufferedReader.readLine()) != null)
                {
                    stringBuilder.append(temp +"\n");
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        public void onProgressUpdate(Void... values){
            super.onProgressUpdate();
        }
        @Override
        public void onPostExecute(String result){
            try{
                workList.clear();
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                int count = 0;

                String workName;
                String average;
                String major;

                while (count < jsonArray.length()){

                    JSONObject object = jsonArray.getJSONObject(count);
                    workName = object.getString("workName");
                    average = object.getString("average");
                    major = object.getString("major");
                    WorkList workNameList = new WorkList( workName, average, major);
                    workList.add(workNameList);
                    count++;

                    if (count ==0){
                        AlertDialog dialog;
                        AlertDialog.Builder builder = new AlertDialog.Builder(WorkListActivity.this);
                        dialog = builder.setMessage("조회된 댓글 없음")
                                .setPositiveButton("확인",null)
                                .create();
                        dialog.show();
                    }
                    workListAdapter.notifyDataSetChanged();
                }
           /*     AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(CommentList.this);
                dialog = builder.setMessage(result)
                        .setPositiveButton("확인",null)
                        .create();
                dialog.show();*/
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}


        /*
    //--------------------------------------단어 오름차순-----------------------------------------
            Button buttonTextAsc = (Button) findViewById(R.id.buttonTextAsc) ;
            buttonTextAsc.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Comparator<ListViewItem> textAsc = new Comparator<ListViewItem>() {
                        @Override public int compare(ListViewItem item1, ListViewItem item2) {
                            return item1.getText().compareTo(item2.getText()) ;
                        }
                    } ;
                    Collections.sort(itemList, textAsc) ;
                    adapter.notifyDataSetChanged() ;
                }
            });

        //----------------------------------단어 내림차순--------------------------------
            Button buttonTextDesc = (Button) findViewById(R.id.buttonTextDesc) ;
            buttonTextDesc.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Comparator<ListViewItem> textDesc = new Comparator<ListViewItem>() {
                        @Override
                        public int compare(ListViewItem item1, ListViewItem item2) {
                            return item2.getText().compareTo(item1.getText()) ;
                        }
                    } ;
                    Collections.sort(itemList, textDesc) ;
                    adapter.notifyDataSetChanged() ;
                }
        });
//----------------------------------숫자 오른차순----------------------------------
        Button buttonNoAsc = (Button) findViewById(R.id.buttonNoAsc) ;
        buttonNoAsc.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Comparator<ListViewItem> noAsc = new Comparator<ListViewItem>() {
                    @Override
                    public int compare(ListViewItem item1, ListViewItem item2) {
                        int ret ;

                        if (item1.getNo() < item2.getNo())
                            ret = -1 ;
                        else if (item1.getNo() == item2.getNo())
                            ret = 0 ;
                        else
                            ret = 1 ;

                        return ret ;

                        // 위의 코드를 간단히 만드는 방법.
                        // return (item1.getNo() - item2.getNo()) ;
                    }
                } ;

                Collections.sort(itemList, noAsc) ;
                adapter.notifyDataSetChanged() ;
            }
        });
//-----------------------숫자 내림차순------------------------------------
        Button buttonNoDesc = (Button) findViewById(R.id.buttonNoDesc) ;
        buttonNoDesc.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Comparator<ListViewItem> noDesc = new Comparator<ListViewItem>() {
                    @Override
                    public int compare(ListViewItem item1, ListViewItem item2) {
                        int ret = 0 ;

                        if (item1.getNo() < item2.getNo())
                            ret = 1 ;
                        else if (item1.getNo() == item2.getNo())
                            ret = 0 ;
                        else
                            ret = -1 ;

                        return ret ;

                        // 위의 코드를 간단히 만드는 방법.
                        // return (item2.getNo() - item1.getNo()) ;
                    }
                } ;

                Collections.sort(itemList, noDesc) ;
                adapter.notifyDataSetChanged() ;
            }
        });

    }

}
*/