package com.example.young.beacon;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by young on 2017-08-10.
 */
//구현은 방학때 시작할걸
public class CommentListAdapter extends BaseAdapter {

    private Context context;
    private List<Comment> commentList;

    public CommentListAdapter(Context context, List<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int i) {
        return commentList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
       View v = View.inflate(context, R.layout.comment, null);

        TextView num= (TextView)v.findViewById(R.id.num);
        TextView comment =(TextView)v.findViewById(R.id.comment);
        TextView star= (TextView)v.findViewById(R.id.star);
        TextView workName = (TextView)v.findViewById(R.id.wN);
        TextView userID = (TextView)v.findViewById(R.id.userID);

        num.setText(commentList.get(i).getNum());
        comment.setText(commentList.get(i).getWorkComment());
        star.setText(commentList.get(i).getStar());
        workName.setText(commentList.get(i).getWorkName());
        userID.setText(commentList.get(i).getUserID());

        v.setTag(commentList.get(i).getWorkComment());
        return v;
    }
}
