package com.example.young.beacon;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Comment;

public class CommentActivity extends AppCompatActivity {
//1학기 계획서가 별로였나보다...
    RatingBar rating_Bar;
    TextView rating_Text,comment_work_name,userID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // final EditText star = (EditText)findViewById(R.id.star); 별점
        // Button button1=(Button)findViewById(R.id.listView_test); 리스트뷰 보기 버튼
        setContentView(R.layout.activity_comment);
        final EditText comment = (EditText)findViewById(R.id.Comment_Text);
        final  Button registerButton = (Button)findViewById(R.id.comment_Button);



        //작품 이름 받아올거임
        Intent intent = getIntent();

        comment_work_name = (TextView)findViewById(R.id.CommentWorkName);
        comment_work_name.setText( intent.getExtras().getString("workName"));
        userID = (TextView)findViewById(R.id.ui);
        GlobalString gUserID = (GlobalString)getApplication();
        userID.setText(gUserID.getGuserID());
       /* userID =(TextView)findViewById(R.id.userID);
        userID .setText(intent.getExtras().getString("userID"));*/
        //레이팅
        rating_Bar = (RatingBar) findViewById(R.id.rating_Bar);
        rating_Text = (TextView) findViewById(R.id.rating_Text);

        rating_Bar.setStepSize((float) 0.5);        //별 색깔이 1칸씩줄어들고 늘어남 0.5로하면 반칸씩 들어감
        rating_Bar.setRating((float) 2.5);      // 처음보여줄때(색깔이 한개도없음) default 값이 0  이다
        rating_Bar.setIsIndicator(false);           //true - 별점만 표시 사용자가 변경 불가 , false - 사용자가 변경가능

        rating_Bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                rating_Text.setText(rating+"");

            }
        });
                registerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String num= "";
                        String wC = comment.getText().toString();
                        String wN = comment_work_name.getText().toString();
                        String UI = userID.getText().toString();


           /* String wN = workName.getText().toString();
            String write = writter.getText().toString();*/
                        String staring =rating_Text.getText().toString();

                if (wC.equals("")||staring.equals(""))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CommentActivity.this);
                    builder.setMessage("댓글과 별점을 확인해 주세요")
                            .setNegativeButton("확인",null)
                            .create()
                            .show();
                }
                else {
                    Response.Listener<String> responseListener = new Response.Listener<String>(){
                        @Override
                        public void onResponse(String reponse){
                            try{
                                JSONObject jsonResponse = new JSONObject(reponse);
                                boolean success = jsonResponse.getBoolean("success");
                                if(success){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(CommentActivity.this);
                                    builder.setMessage("성공.")
                                            .setPositiveButton("확인",null)
                                            .create()
                                            .show();
                                 finish();
                                }
                                else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(CommentActivity.this);
                                    builder.setMessage("실패.")
                                            .setNegativeButton("확인",null)
                                            .create()
                                            .show();
                                }
                            }
                            catch (JSONException e){
                                e.printStackTrace();
                            }

                        }
                    };

                    CommentRequest commentRequest = new CommentRequest(num, wC, wN, UI, staring, responseListener);
                    RequestQueue queue = Volley.newRequestQueue(CommentActivity.this);
                    queue.add(commentRequest);
                }

            }
        });


    }


}
