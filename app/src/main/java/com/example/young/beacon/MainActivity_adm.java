package com.example.young.beacon;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity_adm extends AppCompatActivity {
//김삼근 교수님이 아니였다면 이만큰 완성도 안했을 것이다.
   // private String userPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_adm);
        Intent intent = getIntent();
      //  userPosition = intent.getExtras().getString("userPosition");
        GlobalString Gposition = (GlobalString)getApplication();
        Gposition.setGposition(intent.getExtras().getString("userPosition"));

        //버튼정의
        Button Work_search = (Button) findViewById(R.id.Work_search) ;
        Button Lottery = (Button)findViewById(R.id.Lottery) ;
        Button Show_appraisal = (Button)findViewById(R.id.Show_appraisal);



        //작품검색
        Work_search.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_adm.this, WorkSearch.class);
                startActivity(intent);
            }
        });

        //추첨하기
        Lottery.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "추첨 버튼", Toast.LENGTH_LONG);
                toast.show();
            }
        });

        //평가보기
        Show_appraisal.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_adm.this, WorkListActivityForAdmin.class);
                startActivity(intent);
            }
        });
    }
    private  long lastTimeBackPressed;

    @Override
    public void onBackPressed(){
        if (System.currentTimeMillis() - lastTimeBackPressed<1500)
        {
            ActivityCompat.finishAffinity(this);
            return;
        }
        Toast.makeText(this, "'뒤로' 버튼을 한 번 더 눌러 종료합니다.", Toast.LENGTH_SHORT).show();
        lastTimeBackPressed = System.currentTimeMillis();
    }
}
