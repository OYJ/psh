package com.example.young.beacon;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity_prof extends AppCompatActivity {

//김삼근 교수님 감사합니다.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_prof);
        Intent intent = getIntent();
        //userPosition = intent.getExtras().getString("userPosition");
        GlobalString Gposition = (GlobalString)getApplication();
        Gposition.setGposition( intent.getExtras().getString("userPosition"));
        //버튼정의
        Button Work_search = (Button) findViewById(R.id.Work_search) ;
        Button Work_appraisal = (Button)findViewById(R.id.Work_appraisal);
        Button Work_list = (Button)findViewById(R.id.Work_list);
        Button Beacon = (Button)findViewById(R.id.Beacon);


        //작품 검색
        Work_search.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_prof.this, WorkSearch.class);
                startActivity(intent);
            }
        });

        //작품 평가
        Work_appraisal.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_prof.this, WorkListActivityForAppraisal.class);
                startActivity(intent);

            }
        });
        //작품 리스트
        Work_list.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_prof.this, WorkListActivity.class);
                startActivity(intent);

            }
        });
        //비콘
        Beacon.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "비콘 버튼", Toast.LENGTH_LONG);
                toast.show();

            }
        });
    }
    private  long lastTimeBackPressed;

    @Override
    public void onBackPressed(){
        if (System.currentTimeMillis() - lastTimeBackPressed<1500)
        {
            ActivityCompat.finishAffinity(this);
            return;
        }
        Toast.makeText(this, "'뒤로' 버튼을 한 번 더 눌러 종료합니다.", Toast.LENGTH_SHORT).show();
        lastTimeBackPressed = System.currentTimeMillis();
    }
}

