package com.example.young.beacon;

/**
 * Created by young on 2017-08-24.
 */

public class WorkList {

    String workName;
    String average;
    String major;

    public WorkList(String workName, String average, String major) {
        this.workName = workName;
        this.average = average;
        this.major = major;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
}
