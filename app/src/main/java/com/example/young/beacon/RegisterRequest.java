package com.example.young.beacon;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Young on 2017-07-08.
 */

public class RegisterRequest extends StringRequest {

    final static private String URL = "http://192.168.200.174/Register.php";
    private Map<String, String> parameters;

    public RegisterRequest(String userID, String userPassword, String userName, String userPosition, Response.Listener<String> listener){
        super(Method.POST, URL, listener, null);
        parameters= new HashMap<>();
        parameters.put("userID", userID);
        parameters.put("userPassword", userPassword);
        parameters.put("userName", userName);
        parameters.put("userPosition", userPosition);
    }

    @Override
    public Map<String, String> getParams(){
        return parameters;
    }

}