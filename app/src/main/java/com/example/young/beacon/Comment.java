package com.example.young.beacon;

/**
 * Created by young on 2017-08-16.
 */
// 15팀 중 조사된 10팀 A+ 1팀 A 3팀은 모르고 우리팀은 B+..
public class Comment {
    String num;
    String workComment;
    String workName;
    String userID;
    String star;


    public String getUserID() {
        return userID;
    }


    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getWorkComment() {
        return workComment;
    }

    public void setWorkComment(String workComment) {
        this.workComment = workComment;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    //String num,
    public Comment(String num,String workComment, String workName,String userID, String star) {
        this.num=num;
        this.workComment = workComment;
        this.workName = workName;
        this.star = star;
        this.userID = userID;
    }
}
