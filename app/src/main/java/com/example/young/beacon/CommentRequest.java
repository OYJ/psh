package com.example.young.beacon;


import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Young on 2017-07-26.
 */
//왜 부교수님은 구현을 자꾸 하라고 하셨을까?
public class CommentRequest extends StringRequest {

    final static private String URL = "http://192.168.200.174/Comment.php";
    private Map<String, String> parameters;

    public CommentRequest(String num, String workComment, String workName, String userID, String star, Response.Listener<String> listener){
        super(Method.POST, URL, listener, null);
        parameters= new HashMap<>();
        parameters.put("num", num);
        parameters.put("workComment", workComment);
        parameters.put("workName", workName);
        parameters.put("userID", userID);
        parameters.put("star", star);
    }

    @Override
    public Map<String, String> getParams(){
        return parameters;
    }
}
