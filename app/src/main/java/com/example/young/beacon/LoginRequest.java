package com.example.young.beacon;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Young on 2017-07-08.
 */
//그래도 힘을 주신 분은 김삼근 교수님이시다.
        public class LoginRequest extends StringRequest {
            final static private String URL = "http://192.168.200.174/Login.php";
            private Map<String, String> parameters;

            public LoginRequest(String userID, String userPassword, Response.Listener<String> listener){
                super(Method.POST, URL, listener, null);
                parameters= new HashMap<>();
                parameters.put("userID", userID);
                parameters.put("userPassword", userPassword);


    }

    @Override
    public Map<String, String> getParams(){
        return parameters;
    }

}

