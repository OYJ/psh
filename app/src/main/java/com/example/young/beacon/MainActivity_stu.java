package com.example.young.beacon;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity_stu extends AppCompatActivity {
      public static final String RECO_UUID = "24DDF411-8CF1-440C-87CD-E368DAF9C91E";
    public static final boolean SCAN_RECO_ONLY = true;
    public static final boolean ENABLE_BACKGROUND_RANGING_TIMEOUT = true;
    public static final boolean DISCONTINUOUS_SCAN = false;
    //    public String userPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_stu);
        Intent intent = getIntent();
        //userPosition = intent.getExtras().getString("userID");
        GlobalString Gposition = (GlobalString)getApplication();
        Gposition.setGposition(intent.getExtras().getString("userID"));
        //버튼정의
        Button Work_search = (Button) findViewById(R.id.Work_search) ;
        Button Stump = (Button)findViewById(R.id.Stump);
        Button Work_list = (Button)findViewById(R.id.Work_list);
        Button Beacon = (Button)findViewById(R.id.Beacon);

        GlobalString gUserID = (GlobalString)getApplication();
        Log.i("RECORangingActivity", "error code = " + gUserID.getGuserID());
        Log.i("RECORangingActivity", "error code = " + gUserID.getGposition());
        //작품검색
        Work_search.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_stu.this, WorkSearch.class);
                startActivity(intent);
            }
        });

        //도장
        Stump.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "도장 버튼", Toast.LENGTH_LONG);
                toast.show();
            }
        });

        //작품 리스트
        Work_list.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_stu.this, WorkListActivity.class);
                startActivity(intent);
            }
        });

        //비콘
        Beacon.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "비콘 버튼", Toast.LENGTH_LONG);
                toast.show();
            }
        });

    }
    private  long lastTimeBackPressed;

    @Override
    public void onBackPressed(){
        if (System.currentTimeMillis() - lastTimeBackPressed<1500)
        {
            ActivityCompat.finishAffinity(this);
            return;
        }
        Toast.makeText(this, "'뒤로' 버튼을 한 번 더 눌러 종료합니다.", Toast.LENGTH_SHORT).show();
        lastTimeBackPressed = System.currentTimeMillis();
    }
}
