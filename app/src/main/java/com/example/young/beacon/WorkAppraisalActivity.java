package com.example.young.beacon;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class WorkAppraisalActivity extends AppCompatActivity {
    private int i = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_appraisal);

      /*  final Button backButton = (Button) findViewById(R.id.back);
        final Button nextButton = (Button) findViewById(R.id.next);*/
        final LinearLayout appraisal =(LinearLayout)findViewById(R.id.appraisal);
        TextView workName = (TextView)findViewById(R.id.workName);
        Intent intent = getIntent();
        workName.setText(intent.getExtras().getString("Name"));
       GlobalString globalString = (GlobalString)getApplication();
        globalString.setGworkName(intent.getExtras().getString("Name"));

        appraisal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appraisal.setVisibility(View.GONE);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_appraisal, new WorkAppraisal_1Fragment());
                fragmentTransaction.commit();
            }
        });
    }
}
