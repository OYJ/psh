package com.example.young.beacon;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Work_List_For_Prof_Dialog extends Dialog {

    private TextView mTitleView;
    private TextView mWorkView;
    private Button mAppriaisal;
    private Button mWorkButton;
    private String mTitle;
    private String mWork;

    private View.OnClickListener mCommentClickListener;
    private View.OnClickListener mWorkClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.activity_work__list__dialog_for_prof);

        mTitleView = (TextView) findViewById(R.id.title);
        mWorkView = (TextView) findViewById(R.id.title_WorkName);
        mAppriaisal = (Button) findViewById(R.id.btn_to_Appriaisal);
        mWorkButton = (Button) findViewById(R.id.btn_to_Work);

        // 제목과 내용을 생성자에서 셋팅한다.
        mTitleView.setText(mTitle);
        mWorkView.setText(mWork);

        // 클릭 이벤트 셋팅
        if (mCommentClickListener != null && mWorkClickListener != null) {
            mAppriaisal.setOnClickListener(mCommentClickListener);
            mWorkButton.setOnClickListener(mWorkClickListener);
        }
        else if (mCommentClickListener != null && mWorkClickListener == null) {
            mAppriaisal.setOnClickListener(mCommentClickListener);
        } else {

        }
    }

    // 클릭버튼이 하나일때 생성자 함수로 클릭이벤트를 받는다.
    public Work_List_For_Prof_Dialog(Context context, String title,
                                     View.OnClickListener singleListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mCommentClickListener = singleListener;
    }

    // 클릭버튼이 확인과 취소 두개일때 생성자 함수로 이벤트를 받는다
    public Work_List_For_Prof_Dialog(Context context, String title,
                                     String content, View.OnClickListener leftListener,
                                     View.OnClickListener rightListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mWork = content;
        this.mCommentClickListener = leftListener;
        this.mWorkClickListener = rightListener;
    }
}
