package com.example.young.beacon;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WorkAppraisal_3Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WorkAppraisal_3Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkAppraisal_3Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public WorkAppraisal_3Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WorkAppraisal_3Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WorkAppraisal_3Fragment newInstance(String param1, String param2) {
        WorkAppraisal_3Fragment fragment = new WorkAppraisal_3Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    TextView Q5,Q6;
    EditText A5,A6;
    Button back,submit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_work_appraisal_3,null);
        final GlobalString globalString = (GlobalString)getActivity().getApplication();

        // Inflate the layout for this fragment
        Q5 =(TextView)view.findViewById(R.id.q5);
        Q6 =(TextView)view.findViewById(R.id.q6);
        A5 = (EditText)view.findViewById(R.id.q5_num);
        A6 = (EditText)view.findViewById(R.id.q6_num);
        back = (Button)view.findViewById(R.id.back);
        submit = (Button)view.findViewById(R.id.submit) ;

        Q5.setText("5번 문항");
        Q6.setText("6번 문항");
        A5.setText(globalString.getA5());
        A6.setText(globalString.getA6());
        back.setText("BACK");
        submit.setText("SUBMIT");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalString globalString = (GlobalString)getActivity().getApplication();
                globalString.setA5(A5.getText().toString());
                globalString.setA6(A6.getText().toString());
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_appraisal, new WorkAppraisal_2Fragment());
                fragmentTransaction.commit();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalString.setA5(A5.getText().toString());
                globalString.setA6(A6.getText().toString());

                String userID=globalString.getGuserID();
                String num="";
                String major =  globalString.getGmajor();
                String workName=globalString.getGworkName();
                String a1=globalString.getA1();
                String a2=globalString.getA2();
                String a3=globalString.getA3();
                String a4=globalString.getA4();
               String a5=globalString.getA5();
               String a6=globalString.getA6();
                int avg =(Integer.parseInt(a1)+Integer.parseInt(a2)+Integer.parseInt(a3)+Integer.parseInt(a4)+Integer.parseInt(a5)+Integer.parseInt(a6))/6
                        +(Integer.parseInt(a1)+Integer.parseInt(a2)+Integer.parseInt(a3)+Integer.parseInt(a4)+Integer.parseInt(a5)+Integer.parseInt(a6))%6 ;
                String average = String.valueOf(avg);
                String blank="";
                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse(String reponse){
                        try{
                            JSONObject jsonResponse = new JSONObject(reponse);
                            boolean success = jsonResponse.getBoolean("success");
                            if(success){
                                globalString.setA5("");
                                getActivity().finish();
                              /*  Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                RegisterActivity.this.startActivity(intent);*/
                            }
                            else{

                            }
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                };
                AppraisalRequest appraisalRequest = new AppraisalRequest(num, userID, major, workName, average, blank, responseListener);
                RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
                queue.add(appraisalRequest);
            }
        });



        return view;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p>
 * See the Android Training lesson <a href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</a> for more information.
 */
public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
}
}
