package com.example.young.beacon;


import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
// 나도 하기 싫다...
    private String userID;
    private boolean commentvalidate = false;
    private String gUserID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText idText =(EditText)findViewById(R.id.idText);
        final EditText passwordText =(EditText)findViewById(R.id.passwordText);
        final Button loginButton = (Button)findViewById(R.id.loginButton);
        final TextView registerButton = (TextView)findViewById(R.id.registerButton);

// --------------------테스트 라인-------------------------------------
        final Button testbutton = (Button)findViewById(R.id.test_Button);
//댓글 작성 창

        testbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,WorkAppraisalActivity.class);
                startActivity(intent);
            }
        });
        //댓글 리스트 창
        final Button list = (Button)findViewById(R.id.list);

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent=new Intent(LoginActivity.this,CommentList.class);
                intent.putExtra("workName","컴공2");
                startActivity(intent);
            }
        });


        // ---------------------------여기까지 테스트 ----------------------
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               userID = idText.getText().toString();
                Intent registerintent = new Intent(LoginActivity.this,RegisterActivity.class);

                LoginActivity.this.startActivity(registerintent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String userID = idText.getText().toString();
                final String userPassword = passwordText.getText().toString();

                GlobalString globalString = (GlobalString)getApplication();
                globalString.setGuserID(idText.getText().toString());
                Log.i("RECORangingActivity", "gUserID " + gUserID);


                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            if(success){
                              /*  Intent intent = new Intent(LoginActivity.this, 들어갈곳.class);
                                LoginActivity.this.startActivity(intent);*/

                                String userID = jsonResponse.getString("userID");
                                String userPassword = jsonResponse.getString("userPassword");
                                String userPosition = jsonResponse.getString("userPosition");

                                if (userPosition.equals("학생")){
                                    Intent intent = new Intent(LoginActivity.this, MainActivity_stu.class);
                                    intent.putExtra("uerPosition",userPosition);
                                    LoginActivity.this.startActivity(intent);
                                    }
                                else if (userPosition.equals("관리자")){
                                    Intent intent = new Intent(LoginActivity.this, MainActivity_adm.class);
                                    intent.putExtra("userPosition",userPosition);
                                    LoginActivity.this.startActivity(intent);
                                }
                                else if(userPosition.equals("평가자")){
                                    Intent intent = new Intent(LoginActivity.this, MainActivity_prof.class);
                                    intent.putExtra("userPosition",userPosition);
                                    LoginActivity.this.startActivity(intent);
                                }
                            }
                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage("로그인에 실패하였습니다.")
                                        .setNegativeButton("다시 시도",null)
                                        .create()
                                        .show();
                            }
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                };

                LoginRequest loginRequest = new LoginRequest(userID, userPassword, responseListener);
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(loginRequest);
            }
        });

    }

}
